/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser;

import java.io.File;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import youtube_parser.kernel.MainLogic;

/**
 *
 * @author Betepok
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static boolean inCommandLineMod = false;
    public static MainLogic logic;
    public static void main(String[] args) {
        // TODO code application logic here
        if(args.length > 0){
            try {
                String OS = System.getProperty("os.name").toLowerCase();
                if(OS != null && OS.indexOf("win") >= 0)
                    System.setOut(new PrintStream(System.out,true,"CP866"));
                else
                    System.setOut(new PrintStream(System.out,true,"UTF8"));
            } catch (UnsupportedEncodingException ex) {
                System.out.println("Error setting console encoding!");
            }
            String filepath = "";
            int Depth = 0;
            int GoOverAuthorVidio = 0;
            int getComments = 1;
            if(args[0].equalsIgnoreCase("help")){
                System.out.println("Правильный синтаксис:\nАргумент 1: "
                        + "Путь к файлу с url видео(String Обязательный)\nАргумент 2: Глубина обхода "
                        + "(int >= 0. Значение по умолчанию: 0)\nАргумент 3: Обход по другим видео автора"
                        + "(int 1-Да, 0-Нет. Значение по умолчанию: 0(Нет))\n"
                        +"Аргумент 4: Получение комментарий о видео (int 1-Да, 0-Нет. Значение по умолчанию: 1(Да))"+
                        "\nДля запуска в графическом режиме запустите программу без входных аргументов.");
                return;
            }
            filepath = args[0];
            File f = new File(filepath);
            if(!f.exists()) {
                System.out.println("Введенного файла не существует!");
                return;
            }
            if(args.length>1)
                Depth = Integer.parseInt(args[1]);
            if(args.length>2){
                GoOverAuthorVidio = Integer.parseInt(args[2]);
            }
            if(args.length>2){
                getComments = Integer.parseInt(args[3]);
            }
            if(Depth<0 || (GoOverAuthorVidio!=0 && GoOverAuthorVidio!=1) ||
                    (getComments!=0 && getComments!=1)){
                System.out.println("Ошибка входных данных! Наберите help для просмотра синтаксиса.");
            } else{
               System.out.println("Программа запущена со следующими входными параметрами\n"+
                       "Входной файл: "+f.getAbsolutePath()+"\n"+
                       "Глубина обхода: "+Depth+"\n"+
                       "Обход по другим видео автора: "+GoOverAuthorVidio+"\n"+
                       "Получать комментарии: "+getComments+"\n");
               inCommandLineMod = true;
               logic = new MainLogic();
               logic.startProgram(getComments==1, GoOverAuthorVidio==1, true, Depth, 0, new File(filepath), null, null, null, null, null, null);
               ExecutorService executor = Executors.newFixedThreadPool(1);
               executor.execute(logic);
               executor.shutdown();
            }
        } else {
            Window MainWindow = new Window();
            MainWindow.setTitle("Парсер Метаданных для Youtube");
            MainWindow.setVisible(true);
            MainWindow.setResizable(false);
            MainWindow.initiate(new MainLogic());
        }
    }

}
