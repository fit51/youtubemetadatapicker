/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.FileWriter;
import java.io.IOException;
import youtube_parser.kernel.ParsingObjects.*;

/**
 *
 * @author pavel
 */
public class Saver {

    private static String FileName = "default.txt";
    private static int dods = 0;

    public Saver(String filename) {
        Saver.FileName = filename;
        Saver.save_json_object_or_string(null, null, "{\"objects\":[{\n\"version\":\"1.0\"}", 1);
    }

    public static void save_end() {
        Saver.save_json_object_or_string(null, null, "]}", 1);
    }

    public static void new_file(String dirname) {
        Saver.FileName = dirname+"/"+"json.txt";
        Saver.save_json_object_or_string(null, null, "{\"objects\":[{\"version\":\"1.0\"}", 1);
    }

    public static int save_author(Author AuthorObject) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();;
        return save_json_object_or_string(gson, AuthorObject, null, 0);
    }

    public static int save_authorvideo(AuthorVideo Video) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();;
        return save_json_object_or_string(gson, Video, null, 0);
    }

    public static int save_video(VidioObject Video) {
        Gson gson = new GsonBuilder().registerTypeAdapter(VidioObject.class, new VidioJsonSerializer()).
                setPrettyPrinting().create();
        return save_json_object_or_string(gson, Video, null, 0);
    }
    
    private static synchronized <T> int save_json_object_or_string(Gson gson, T ob, String buffer, int mode) {
//        try {
        //MainLogic.synchronizedlogging.WriteToLog("Saving result...");
        if(mode == 0){
            String ob_delimetr = ",\n";
            try(FileWriter FWOut = new FileWriter(Saver.FileName, true)){
                FWOut.write(ob_delimetr);
                gson.toJson(ob, FWOut);
                gson = null;
            } catch (IOException ex) {
                System.err.println("Saver Error: Can`t save object");
                System.err.println("Error: " + ex.getMessage());
                return -1;
            }
        } else
        {
            try(FileWriter FWOut = new FileWriter(Saver.FileName, true);) {
                FWOut.write(buffer);
            } catch (IOException e) {
                System.err.println("Saver Error: Can`t save object save_string");
                System.err.println("Error: " + e.getMessage());
            return -1;
        }
        return 0;
        }
//            FWOut.close(); 
//            FWOut = new FileWriter(Saver.FileName, true);
//            gson.toJson(ob, FWOut);
//            FWOut.close(); 
            
//        } catch (IOException e) {
//            System.err.println("Saver Error: Can`t save object");
//            System.err.println("Error: " + e.getMessage());
//            return -1;
//        }
        return 0;
    }

//    private static synchronized int save_string(String buffer) {
//        try(FileWriter FWOut = new FileWriter(Saver.FileName, true);) {
//            FWOut.write(buffer);
//        } catch (IOException e) {
//            System.err.println("Saver Error: Can`t save object save_string");
//            System.err.println("Error: " + e.getMessage());
//            return -1;
//        }
//        return 0;
//    }

}
