/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package youtube_parser.kernel;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import youtube_parser.kernel.ParsingObjects.VidioObject;

/**
 *
 * @author pavel
 */
public class VidioJsonSerializer implements JsonSerializer<VidioObject> {

    @Override
    public JsonElement serialize(VidioObject foo, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();
        object.add("type", context.serialize(foo.type));
        object.add("title", context.serialize(foo.title));
        object.add("id", context.serialize(foo.id));
        object.add("date", context.serialize(foo.date));
        object.add("views", context.serialize(foo.views));
        object.add("likes", context.serialize(foo.likes));
        object.add("dislikes", context.serialize(foo.dislikes));
        object.add("authorid", context.serialize(foo.authorid));
        object.add("author", context.serialize(foo.author));
        object.add("authorcomment", context.serialize(foo.authorcomment));
        object.add("commentscount", context.serialize(foo.commentscount));
        object.add("commentsurl", context.serialize(foo.commentsurl));
        object.add("comments", context.serialize(foo.comments));
        return object;
    }

}
