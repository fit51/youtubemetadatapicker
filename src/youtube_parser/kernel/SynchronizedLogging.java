/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel;

import java.awt.Label;
import java.awt.TextField;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.logging.Level;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import youtube_parser.Main;

/**
 *
 * @author pavel
 */
public class SynchronizedLogging {

    private final JTextArea Logger;
    private final Label AllQueue;
    private final Label DoneQueue;
    private static String ErrorFile;
    private static String LogFile;
    private int numberofdone;
    private int numberofall;
    private static int numofStrings = 0;
    

    public SynchronizedLogging(JTextArea log, String dir, Label AllQueue, Label DoneQueue) {
        Logger = log;
        this.AllQueue = AllQueue;
        this.DoneQueue = DoneQueue;
        ErrorFile = dir+"/"+"ErrorLog.txt";
        LogFile = dir+"/"+"Log.txt";
        numberofall = 0;
        numberofdone = 0;
    }

public void WriteToLog(String s) {
        
        WritetoLogFile(s);
        boolean clean = ChangeNumOfStringsInLog();
        if(Main.inCommandLineMod){
            System.out.println(s+"\n");
        } else{
            SwingUtilities.invokeLater(new WriteLog(s, clean));
        }
//        System.out.println(s);
    }
    
    public synchronized boolean ChangeNumOfStringsInLog(){
        numofStrings++;
        if(numofStrings > 1000) {
            numofStrings = 0;
            return true;
        }
        return false;
    } 
    
    public static synchronized void SetOutOfQuoteApi(){
        WritetoErrorLogFile("You have run out of Google Api Quote!");
        MainLogic.OutGoogleQuoute = true;
    }
    
    public synchronized void WriteNumber(int numb, int Type) {
        int number = 0;
        switch(Type){
            case 0:
                number = ++numberofall;
                break;
            case 1:
                number = ++numberofdone;
                break;
        }
        if(numb != -1)
            number = numb;
        if(Main.inCommandLineMod){
            System.out.println(""+"\n");
        } else{
            SwingUtilities.invokeLater(new WriteNumberOfTasks(number, Type));
        }
//        System.out.println(s);
    }

    public static synchronized void WritetoErrorLogFile(String s) {
        //MainLogic.synchronizedlogging.WriteToLog("Saving error...");
        try (FileWriter FWOut = new FileWriter(ErrorFile, true)){
            FWOut.write(s + "\n");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(SynchronizedLogging.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static synchronized void WritetoLogFile(String s) {
        try (FileWriter FWOut = new FileWriter(LogFile, true)){
            FWOut.write(s + "\n");
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(SynchronizedLogging.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public class WriteLog implements Runnable {
        
        String s;
        boolean clean;

        public WriteLog(String s, boolean clean) {
            this.s = s;
            this.clean = clean;
        }

        @Override
        public void run() {
//            numberofstrings++;
//            if(numberofstrings > 500){
//                numberofstrings = 0;
//                Logger.setText("");
//            }
            if(clean) {
                Logger.setText("");
            }
            Logger.append(s + "\n");
        }

    }
   public class WriteNumberOfTasks implements Runnable {

        String s;
        int Type;

        public WriteNumberOfTasks(int number, int Type) {
            this.s = String.valueOf(number);
            this.Type = Type;
        }

        @Override
        public void run() {
            switch(Type){
                case 0:
                    AllQueue.setText(s + "\n");
                break;
                case 1:
                    DoneQueue.setText(s + "\n");
                break;
            }
        }
   }
}
