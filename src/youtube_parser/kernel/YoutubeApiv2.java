package youtube_parser.kernel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pavel
 */
//Youtube API v 2.0
public class YoutubeApiv2 {

    // returns href on video data
    public static String GetVideoInfo(String video) {
        return "http://gdata.youtube.com/feeds/api/videos/" + video + "?alt=json";
    }

    // returns href on video data
    public static String GetAuthorInfo(String author) {
        return "http://gdata.youtube.com/feeds/api/users/" + author + "?alt=json";
    }
    
    // returns href on video data
    public static String GetGoogleReplies(String id) {
        return "https://www.googleapis.com/plus/v1/activities/"+ id +"/comments?key=" + MainLogic.GoogleApiKey+ "&alt=json";
    }
    
    public static String GetGoogleAuthor(String id) {
        return "https://www.googleapis.com/plus/v1/people/"+ id +"?key=" + MainLogic.GoogleApiKey+ "&alt=json";
    }
}
