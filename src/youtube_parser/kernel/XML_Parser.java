/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XML_Parser {

    public static void ParseVidio(String xml_string) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml_string));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("employee");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);
                NodeList name = element.getElementsByTagName("name");
                Element line = (Element) name.item(0);
                System.out.println("Name: " + getCharacterDataFromElement(line));
                NodeList title = element.getElementsByTagName("title");
                line = (Element) title.item(0);
                System.out.println("Title: " + getCharacterDataFromElement(line));
            }
        } catch (Exception e) {
            System.err.println("Parse error");
        }
    }

    public static void XML_String_Parse(String xml_string) {
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml_string));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("employee");

            for (int i = 0; i < nodes.getLength(); i++) {
                Element element = (Element) nodes.item(i);
                NodeList name = element.getElementsByTagName("name");
                Element line = (Element) name.item(0);
                System.out.println("Name: " + getCharacterDataFromElement(line));
                NodeList title = element.getElementsByTagName("title");
                line = (Element) title.item(0);
                System.out.println("Title: " + getCharacterDataFromElement(line));
            }
        } catch (Exception e) {
            System.err.println("Parse error");
        }
    }

    public static String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
            CharacterData cd = (CharacterData) child;
            return cd.getData();
        }
        return "";
    }
}
