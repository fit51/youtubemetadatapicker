package youtube_parser.kernel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.Charset;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.util.concurrent.*;
import org.apache.http.client.fluent.Request;

public class Socket_client {

    private int RESPONSE_CODE;
    private String URL;
    private String LOCAL_BUFFER;
    private final String USER_AGENT;
    private final int MAX_TRY;

    public Socket_client() {
        this.MAX_TRY = 3;
        this.USER_AGENT = "Mozilla/5.0";
        this.URL = "";
        this.RESPONSE_CODE = 0;
    }

    public int get_response_code() {
        return this.RESPONSE_CODE;
    }

    public String get_response() {
        return this.LOCAL_BUFFER;
    }

    public int send_request(String url) {
        this.send_request_local(url);
//        if (this.LOCAL_BUFFER==null || "".equals(this.LOCAL_BUFFER))
//            return -1;
        if (this.LOCAL_BUFFER==null || "".equals(this.LOCAL_BUFFER)) {
            for (int i = 0; i < this.MAX_TRY; ++i) {
                send_request_local(url);
                if (this.LOCAL_BUFFER!=null && !"".equals(this.LOCAL_BUFFER)) {
                    return 1;
                }
            }
            //System.err.println("Request error");
            //System.err.println(this.RESPONSE_CODE);
            //if (this.RESPONSE_CODE != 404) {
                MainLogic.synchronizedlogging.WritetoErrorLogFile("Request error " + this.RESPONSE_CODE + " - " + url);
            //}
            return -1;
        }
        return 1;
    }

    private int send_request_local(String url) {
        //MainLogic.synchronizedlogging.WriteToLog("Sending request - "+ url);
        try {

                this.URL = url;
                if (URL.isEmpty()) {
                    System.err.println("URL is empty");
                    SynchronizedLogging.WritetoErrorLogFile("URL is empty - " + url);
                    return -2;
                }

                // блокировка была здесь
                // readLine ждет когда появиться конец строки, если по каким-либо причинам конца нет - тупо ждем и процесс висит

            this.LOCAL_BUFFER = Request.Get(url)
                    .execute().returnContent().asString();



                return 1;

        } catch (Exception e) {
            MainLogic.synchronizedlogging.WritetoErrorLogFile("Error sending request - " + url);
            return -1;
        }
    }



}