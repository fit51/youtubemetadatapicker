/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel;

/**
 *
 * @author pavel
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import youtube_parser.kernel.ParsingObjects.Author;
import youtube_parser.kernel.ParsingObjects.AuthorVideo;
import youtube_parser.kernel.ParsingObjects.Channels;
import youtube_parser.kernel.ParsingObjects.Comment;
import youtube_parser.kernel.ParsingObjects.VidioObject;

public class JsonToObjects {

    public VidioObject ParseVideo(String data, String id, JSONObject ob) {
        VidioObject v = new VidioObject();
        v.id = id;
        try {
            JSONObject entry;
            if (data != null) {
                JSONObject json = new JSONObject(data);
                entry = json.getJSONObject("entry"); // this is the "data": { } part
            } else {
                entry = ob;
            }
            v.title = entry.getJSONObject("title").getString("$t");
//            JSONArray ar = entry.getJSONArray("author");
//            JSONObject ob = ar.getJSONObject(0);
//            JSONObject o = ob.getJSONObject("uri");
            try {
                JSONObject comments = entry.getJSONObject("gd$comments").getJSONObject("gd$feedLink");
                v.commentsurl = comments.getString("href");
                v.commentscount = comments.getInt("countHint");
                comments = null;
            } catch (JSONException e) {

            }
            String[] parts = entry.getJSONArray("author").getJSONObject(0).getJSONObject("uri").getString("$t").split("/");
            v.authorid = parts[parts.length - 1];
            parts = null;

            v.author = entry.getJSONArray("author").getJSONObject(0).getJSONObject("name").getString("$t");
            v.date = entry.getJSONObject("published").getString("$t");
            if (entry.getJSONObject("content").getString("type").equalsIgnoreCase("text")) {
                v.authorcomment = entry.getJSONObject("content").getString("$t");
            } else {
                v.authorcomment = "";
            }
            JSONObject rating = null;
            try {
                rating = entry.getJSONObject("gd$rating");
                if (rating != null) {
                    int numRaters = rating.getInt("numRaters");
                    int maxRate = rating.getInt("max");
                    int minRate = rating.getInt("min");
                    double averageRate = rating.getDouble("average");
                    v.likes = (int) (((averageRate - minRate) / (maxRate - minRate)) * numRaters);
                    v.dislikes = (int) ((1 - (averageRate - minRate) / (maxRate - minRate)) * numRaters);
                }
            } catch (JSONException e) {
                v.likes = v.dislikes = 0;
            }
            finally {
                rating = null;
            }
            v.views = entry.getJSONObject("yt$statistics").getInt("viewCount");

        } catch (JSONException e) {
            System.err.println("Error: ParseVideo");
            MainLogic.synchronizedlogging.WriteToLog("Ошибка при парсинге видео с id = " + id + ":\n"
                    + "Детальная информация по ошибке:" + e.getMessage());
            MainLogic.synchronizedlogging.WritetoErrorLogFile("Error parsing vidio with id = " + id + ":\n"
                    + "Error detail:" + e.getMessage());
            return null;
        }
        return v;
    }

    public void ParseVideoComment(ArrayList<Comment> res, String data, int page_id) {
        try {
            JSONObject json = new JSONObject(data);
            data = null;
            JSONObject feed = json.getJSONObject("feed");
            json = null;

            if(feed.has("entry")){
                JSONArray entries = feed.getJSONArray("entry");
                feed = null;
                for (int i = 0; i < entries.length(); i++) {
                    Comment comment = new Comment();
                    JSONObject entry = entries.getJSONObject(i);
                    String[] parts = entry.getJSONObject("id").getString("$t").split("/");
                    comment.id = parts[parts.length - 1];
                    parts = null;
                    comment.text = entry.getJSONObject("content").getString("$t");
                    parts = entry.getJSONArray("author").getJSONObject(0).getJSONObject("uri").getString("$t").split("/");
                    comment.authorid = parts[parts.length - 1];
                    parts = null;
                    comment.authorname = entry.getJSONArray("author").getJSONObject(0).getJSONObject("name").getString("$t");
                    comment.date = entry.getJSONObject("published").getString("$t");
                    int count = entry.getJSONObject("yt$replyCount").getInt("$t");
                    if(!MainLogic.OutGoogleQuoute && MainLogic.GatherReplies 
                            && !MainLogic.GoogleApiKey.isEmpty() && count != 0)
                        getCommentReplies(comment);
                    res.add(comment);
                }
            }
        } catch (JSONException e) {
            System.err.println("Error: ParseVideoComment");
            e.printStackTrace();
            MainLogic.synchronizedlogging.WriteToLog("Ошибка: Парсинг Комментариев к видео. Детали ошибки: " + e.getMessage());
        }
    }
    
    public void getCommentReplies(Comment comment){
        try{
            Socket_client http = new Socket_client();
            if (http.send_request(YoutubeApiv2.GetGoogleReplies(comment.id)) == 1) // запрашиваем инфу об авторе
            {
                ArrayList<Comment> list = new ArrayList<>();
                String data = http.get_response();

                http = null;

                JSONObject json = new JSONObject(data);
                JSONArray items = json.getJSONArray("items");

                data = null;
                json = null;

                Map<String, String> authors;
                authors = new HashMap<>();
                for (int i = 0; i < items.length(); i++) {
                    Comment c = new Comment();
                    JSONObject item = items.getJSONObject(i);
                    c.text = item.getJSONObject("object").getString("content");
                    c.date = item.getString("published");
                    c.id = item.getString("id");
                    c.authorname = item.getJSONObject("actor").getString("displayName");
                    String userid = item.getJSONObject("actor").getString("id");
                    if (authors.containsKey(userid))
                        c.authorid = authors.get(userid);
                    else {
                        http = new Socket_client();
                        if (http.send_request(YoutubeApiv2.GetGoogleAuthor(userid)) == 1) {
                            String data2 = http.get_response();
                            JSONObject obj = new JSONObject(data2);

                            http = null;
                            data2 = null;

                            if (obj.has("urls")) {
                                JSONArray urls = obj.getJSONArray("urls");

                                obj = null;

                                for (int j = 0; j < urls.length(); j++) {
                                    String url = urls.getJSONObject(j).getString("value");
                                    if (url.contains("youtube")) {
                                        String[] str = url.split("/");
                                        c.authorid = str[str.length - 1];
                                        authors.put(userid, c.authorid);

                                        str = null;

                                        break;
                                    }
                                }
                            }
                        } else {
                            SynchronizedLogging.SetOutOfQuoteApi();
                            list.add(c);
                            comment.replies = list;
                            return;
                        }
                    }

                    list.add(c);
                }
                comment.replies = list;
            }  else {
                SynchronizedLogging.SetOutOfQuoteApi();
            }         
        
        } catch(JSONException e){
            System.err.println("Error: getCommentReplies");
        }
    }

    public ArrayList<Comment> Parse25VideoComment(String data) {
        ArrayList<Comment> res = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(data);
            JSONObject feed = json.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entry");
            for (int i = 0; i < entries.length(); i++) {
                Comment comment = new Comment();
                JSONObject entry = entries.getJSONObject(i);
                String[] parts = entry.getJSONObject("id").getString("$t").split("/");
                comment.id = parts[parts.length - 1];
                comment.text = entry.getJSONObject("content").getString("$t");
                parts = entry.getJSONArray("author").getJSONObject(0).getJSONObject("uri").getString("$t").split("/");
                comment.authorid = parts[parts.length - 1];
                comment.authorname = entry.getJSONArray("author").getJSONObject(0).getJSONObject("name").getString("$t");
                comment.date = entry.getJSONObject("published").getString("$t");
                res.add(comment);
            }
        } catch (JSONException e) {
            System.err.println("Error: ParseVideoComment");

        }
        return res;
    }

    public Author ParseAuthor(String data, String id) {
        Author AuthorObject = new Author();
        AuthorObject.id = id;
        try {
            JSONObject json = new JSONObject(data);
            JSONObject entry = json.getJSONObject("entry");

            data = null;
            json = null;

            AuthorObject.nickname = entry.getJSONObject("yt$username").getString("$t");
            try {
                AuthorObject.googleplus = entry.getJSONObject("yt$googlePlusUserId").getString("$t");
            } catch (JSONException e) {

            }

            String date =  entry.getJSONObject("published").getString("$t");
            String[] datas = date.split("T");

            AuthorObject.registration_date = datas[0];

            date = null;
            datas = null;

            AuthorObject.text = entry.getJSONObject("content").getString("$t");
            AuthorObject.subscriber_count = entry.getJSONObject("yt$statistics").getString("subscriberCount");
            JSONArray channels = entry.getJSONArray("gd$feedLink");

            entry = null;

            String Url_videos = null;
            int Video_count = 0;
            for (int i = 0; i < channels.length(); i++) {
                JSONObject channel = channels.getJSONObject(i);
                if (channel.getString("rel").split("#")[1].equals("user.uploads")) {
                    AuthorObject.url_videos = channel.getString("href");
                    AuthorObject.video_count = channel.getInt("countHint");
                }
                channel = null;
            }
            AuthorObject.url_channels = ""; // С Каналами пока не понятно
            // AuthorObject - информация об авторе
            // AuthorVideoObject - массив видео
        } catch (JSONException e) {
            System.err.println("Error: ParseAuthor");
            MainLogic.synchronizedlogging.WriteToLog("Ошибка: Парсинг комментария к видео с id = " + id + ": Детали ошибки: " + e.getMessage());
            return null;

        }
        return AuthorObject;
    }

    public void ParseAuthorVideo(ArrayList<JSONObject> res, String data, int page_id) {
        try {
            // иногда попадают нулевые значения
            if (data==null || data.length()==0 || res == null)
                return;

            JSONObject json = new JSONObject(data);
            data = null;
            if(json.has("feed")){
                JSONObject feed = json.getJSONObject("feed");
                json = null;
                if(feed.has("entry")){
                    JSONArray entries = feed.getJSONArray("entry");
                    feed = null;
                    for (int i = 0; i < entries.length(); i++) {
                        JSONObject entry = entries.getJSONObject(i);
                        res.add(entry);
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("Error: ParseAuthorVideo");
            e.printStackTrace();
        }
    }

    public AuthorVideo ParseAuthorVideoEntity(String data, String id, JSONObject ob) {
        AuthorVideo v = new AuthorVideo();
        v.id = id;
        try {

            JSONObject entry;
            if (data != null) {
                JSONObject json = new JSONObject(data);
                entry = json.getJSONObject("entry"); // this is the "data": { } part
            } else {
                entry = ob;
            }

            v.title = entry.getJSONObject("title").getString("$t");
            v.date = entry.getJSONObject("published").getString("$t");
            try {
                v.views = entry.getJSONObject("yt$statistics").getInt("viewCount");
            } catch (JSONException e) {
                v.views = -1;
            }
            try {
                v.commentsurl = entry.getJSONObject("gd$comments").getJSONObject("gd$feedLink").getString("href");
                v.commentscount = entry.getJSONObject("gd$comments").getJSONObject("gd$feedLink").getInt("countHint");
            } catch (JSONException e) {

            }

        } catch (JSONException e) {
            System.err.println("Error: ParseAuthorVideoEntity");
            MainLogic.synchronizedlogging.WriteToLog("Ошибка: Парсинг видео с id = " + id + ": Детали ошибки: " + e.getMessage());
            return null;
        }
        return v;
    }

    public void ParseAuthorVideo2(ArrayList<AuthorVideo> res, String data, int page_id) {
        try {
            JSONObject json = new JSONObject(data);
            JSONObject feed = json.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entry");
            for (int i = 0; i < entries.length(); i++) {
                JSONObject entry = entries.getJSONObject(i);
                AuthorVideo v = new AuthorVideo();
                v.title = entry.getJSONObject("title").getString("$t");
                v.date = entry.getJSONObject("published").getString("$t");
                v.views = entry.getJSONObject("yt$statistics").getInt("viewCount");
                res.add(v);
            }
        } catch (JSONException e) {
            System.err.println("Error: ParseAuthorVideo");
        }
    }

    public ArrayList<JSONObject> Parse25AuthorVideo(String data) {
        ArrayList<JSONObject> res = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(data);
            JSONObject feed = json.getJSONObject("feed");
            JSONArray entries = feed.getJSONArray("entry");
            for (int i = 0; i < entries.length(); i++) {
                JSONObject entry = entries.getJSONObject(i);
                res.add(entry);
            }
        } catch (JSONException e) {
            System.err.println("Error: ParseAuthorVideo");
        }
        return res;
    }

    public ArrayList<Channels> ParseChannels(String id, Socket_client http) {
        //Socket_client http = new Socket_client();
        //String id = "Z7dc2LlkePv0oOfYQER5SA";
        ArrayList<Channels> ChannelsObject = new ArrayList<>();
        try {
            String result = "";
            http.send_request("http://www.youtube.com/channel/UC" + id + "/channels");
            if (http.get_response_code() == 404) {
                http.send_request("http://www.youtube.com/user/" + id + "/channels");
                if (http.get_response_code() == 404) {
                    MainLogic.synchronizedlogging.WritetoErrorLogFile("Request error " + http.get_response_code() + " - " + "http://www.youtube.com/user/" + id + "/channels");
                }
            }
            result = http.get_response();
            String[] parts1 = result.split("<li class=\"channels-content-item yt-shelf-grid-item\">");
            if (parts1.length > 1) {
                for (int i = 1; i < parts1.length; ++i) {
                    Channels localChannel = new Channels();
                    localChannel.count = "null";
                    localChannel.name = "null";
                    localChannel.path = "null";
                    String[] parts2 = parts1[i].split("<a class=\"yt-uix-sessionlink yt-uix-tile-link  spf-link  yt-ui-ellipsis yt-ui-ellipsis-2\" dir=\"ltr\" title=\"");
                    String[] parts3 = parts2[1].split("\" data-sessionlink=");
                    String title = parts3[0];
                    localChannel.name = title;
                    //System.out.println(title);
                    String count = "";
                    String[] parts5 = null;
                    String[] parts4 = parts1[i].split("<span class=\"yt-subscription-button-subscriber-count-unbranded-vertical yt-uix-tooltip\" title=\"");
                    if (parts4.length == 1) {
                        parts4 = parts1[i].split("<span class=\"yt-subscription-button-subscriber-count-unbranded-vertical\" >");
                        if (parts4.length == 1) {
                            count = "undefined";
                            localChannel.count = count;
                            ChannelsObject.add(localChannel);
                            continue;
                        }
                        parts5 = parts4[1].split("</span>");
                    } else {
                        parts5 = parts4[1].split(" ");
                    }
                    count = parts5[0];
                    localChannel.count = count;
                    String[] parts6 = parts1[i].split("<a href=\"");
                    String[] parts7 = parts6[1].split("\"");
                    localChannel.path = parts7[0];
                    ChannelsObject.add(localChannel);
                }
            }
            return ChannelsObject;
        } catch (Exception e) {
            System.out.println("Channel parse fail");
            System.out.println("Exception: " + e.getMessage());
            System.out.println("id: " + id);
            MainLogic.synchronizedlogging.WriteToLog("Ошибка: Парсинг канала автора с id = " + id + ": Детали ошибки: " + e.getMessage());
            return ChannelsObject;
        }
    }
}
