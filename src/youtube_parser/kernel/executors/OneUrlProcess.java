/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel.executors;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import youtube_parser.kernel.JsonToObjects;
import youtube_parser.kernel.MainLogic;
import youtube_parser.kernel.ParsingObjects.Author;
import youtube_parser.kernel.ParsingObjects.AuthorVideo;
import youtube_parser.kernel.ParsingObjects.Channels;
import youtube_parser.kernel.ParsingObjects.Comment;
import youtube_parser.kernel.ParsingObjects.VidioObject;
import youtube_parser.kernel.Saver;
import youtube_parser.kernel.Socket_client;
import youtube_parser.kernel.SynchronizedLogging;

/**
 *
 * @author pavel
 */
public class OneUrlProcess implements Callable {

    private final String url;
    private final int Type, Depth, MaxDepth;
    private final boolean is_First;
    //private Socket_client http;
    // не будем кешировать данные, будем переполучать, да, это снизит производительность, но освободит память
    //private final JSONObject ob;

    public OneUrlProcess(String url, int Type, int Depth, int MaxDepth, boolean is_First) {
        this.url = url;
        this.Type = Type;
        this.Depth = Depth;
        this.MaxDepth = MaxDepth;
        this.is_First = is_First;
        //this.ob = ob;
        //http = new Socket_client();
    }

    @Override
    public ResultOfUrlThread call() throws Exception {
        ResultOfUrlThread r = new ResultOfUrlThread();
        r.list = new ArrayList<>();
        r.Message = url + "Message return";

        if (Thread.currentThread().isInterrupted())
            return r;

        switch (Type) {
            case 0:
                try{
                    VidioMethod(r, null);//исходные видео
                } catch(Exception e){
                    System.err.print("Unhendled Exception 1 - "+ e.getMessage());
                    e.printStackTrace();
                }
                break;
                
            case 1:
                try{
                AuthorMethod(r);//автор
                } catch(Exception e){
                    System.err.print("Unhendled Exception 2 - " + e.getMessage());
                    e.printStackTrace();
                }
                break;
            case 2:
                try{
                VidioMethod(r, null);//видео авторов
                } catch(Exception e){
                    System.err.print("Unhendled Exception 3 - " + e.getMessage());
                    e.printStackTrace();
                }
        }
        MainLogic.synchronizedlogging.WriteNumber(-1, 1);
        return r;
    }

    private void VidioMethod(ResultOfUrlThread r, JSONObject ob) {
        String video = url;
        ArrayList<Comment> VideoComment = null;
        AuthorVideo v = null;
        int res = 0;

        if (Thread.currentThread().isInterrupted())
            return;

        Socket_client http = null;
        if (ob == null) {
            http = new Socket_client();
            res = http.send_request(youtube_parser.kernel.YoutubeApiv2.GetVideoInfo(video)); // собираем инфу о видео
        }
        if (ob != null || res == 1) {

            if (Thread.currentThread().isInterrupted())
                return;

            MainLogic.synchronizedlogging.WriteToLog("Успешное получение данных о видео " + video); // пишем в лог
            String vidiodata = null;

            JsonToObjects jsonToObject = new JsonToObjects();

            if (ob == null) {
                vidiodata = http.get_response(); // получаем ответ видео
            }

            if (Depth == 0) {
                v = jsonToObject.ParseVideo(vidiodata, video, ob); // парсим ответ
            }
             else {
                v = jsonToObject.ParseAuthorVideoEntity(vidiodata, video, ob); // парсим ответ
            }

            jsonToObject = null;

            if(v == null)
                return;
            MainLogic.synchronizedlogging.WriteToLog("Парсинг видео с заголовком " + video/*v.title*/); // пишем в лог
            VideoComment = new ArrayList<>();
            if (v.commentsurl != null && v.commentscount > 0 && MainLogic.getComments) // если комменты есть и их больше 0 и стоит флаг получать комментарии
            {
                if (Thread.currentThread().isInterrupted())
                    return;

                http = null;
                http = new Socket_client();
                res = http.send_request(v.commentsurl + "?alt=json&orderby=published&max-results=50");
                if (res == 1) {                    
                    try {
                        JSONObject json = new JSONObject(http.get_response());
                        JSONObject feed = json.getJSONObject("feed");

                        jsonToObject = new JsonToObjects();

                        if (feed.getJSONObject("openSearch$totalResults").getInt("$t") != 0) {
                            jsonToObject.ParseVideoComment(VideoComment, http.get_response(), 1);
                            jsonToObject = null;

                            JSONArray links = feed.getJSONArray("link");
                            String next = "";
                            try {
                                next = links.getJSONObject(links.length()-1).getString("href");
                            } catch (JSONException e) {
                                System.err.println("Err in Video Method ");
                            }
                            for (int i = 50; i < (v.commentscount > 1000 ? 1000 : v.commentscount); i += 50) {
                                if (Thread.currentThread().isInterrupted())
                                    break;



                                MainLogic.synchronizedlogging.WriteToLog("Получение коммен к видео " + v.title + i);
                                try {
                                    if (next.isEmpty()) {
                                        http = null;
                                        break;
                                    }

                                    if (Thread.currentThread().isInterrupted())
                                        break;

                                    http = null;
                                    http = new Socket_client();

                                    if(http.send_request(next) != 1) {
                                        http = null;
                                        break;
                                    }
                                    json = new JSONObject(http.get_response());
                                    feed = json.getJSONObject("feed");
                                    if (feed.getJSONObject("openSearch$totalResults").getInt("$t") == 0) {
                                        http = null;
                                        break;
                                    }
                                    jsonToObject = new JsonToObjects();
                                    jsonToObject.ParseVideoComment(VideoComment, http.get_response(), i);

                                    http = null;

                                    jsonToObject = null;
                                    links = feed.getJSONArray("link");
                                    next = links.getJSONObject(links.length()-1).getString("href");
                                } catch (JSONException e) {
                                    System.err.println("Err in Video Method 2");
                                    http = null;
                                    break;
                                }
                            }
                            // VideoComment - Comment
                            // v - AuthorVidio
                        }
                    } catch (JSONException e) {
                        MainLogic.synchronizedlogging.WritetoErrorLogFile("Error getting number comments");
                        System.err.println("Error getting number comments");
                    }
                }
            }

            if (Thread.currentThread().isInterrupted())
                return;

            MainLogic.synchronizedlogging.WriteToLog("Сохранение видео "+ video/*v.title*/);
            v.comments = VideoComment;
            if (Depth == 0) {
                Saver.save_video((VidioObject) v);
            } else {
                Saver.save_authorvideo(v);
            }
            
        } else {
            SynchronizedLogging.WritetoErrorLogFile("Error getting video " + video);
            return;
//            MainLogic.synchronizedlogging.WriteToLog("Error getting video " + video);
        }
        ArrayList<OneUrlProcess> list = new ArrayList<>();
        r.list = list;
        if (is_First == true && MainLogic.GoOverAuthorVidios) //добавляем автора самого видео
        {
            list.add(new OneUrlProcess(((VidioObject) v).authorid, 1, Depth, MaxDepth, false));
        }
        if (Depth == MaxDepth || VideoComment == null) {
            return;
        }
        for (Comment c : VideoComment) {
            if (Thread.currentThread().isInterrupted())
                break;

            if(c == null)
                continue;
            if(c.authorid == null)
                continue;
            if (!MainLogic.AlreadyParsedAuth.contains(c.authorid)) // проверка что автора еще не обрабатывали.
            {
                MainLogic.SynAlreadyParsedAuthors(1, c.authorid);
                list.add(new OneUrlProcess(c.authorid, 1, Depth + 1, MaxDepth, false));
            }
        }
    }

    private void AuthorMethod(ResultOfUrlThread r) {
        String author = url;
        ArrayList<JSONObject> AuthorVideoObject = null;
        ArrayList<Channels> Channels = null;

        Socket_client http = new Socket_client();

        if (http.send_request(youtube_parser.kernel.YoutubeApiv2.GetAuthorInfo(author)) == 1) // запрашиваем инфу об авторе
        {
            if (Thread.currentThread().isInterrupted())
                return;

            MainLogic.synchronizedlogging.WriteToLog("Успешно получены данные об авторе " + author); // пишем в лог
            String authordata = http.get_response(); // получаем ответ
            JsonToObjects jsonToObject = new JsonToObjects();
            Author a = jsonToObject.ParseAuthor(authordata, author); // парсим ответ
            jsonToObject = null;
            //Channels = JsonToObjects.ParseChannels(a.id, http); // парсим каналы ---------------------------------------------------
            //a.channels = Channels;
            MainLogic.synchronizedlogging.WriteToLog("Сохранение автора - " + author/*a.nickname*/);
            Saver.save_author(a);
             // пишем в лог
            http = null;
            http = new Socket_client();
            int res = http.send_request(a.url_videos + "?alt=json"); // получаем страницу с видео автора
            if (res == 1) {
                //AuthorVideoObject = JsonToObjects.Parse25AuthorVideo(http.get_response());

                ArrayList<OneUrlProcess> list = new ArrayList<>();
                r.list = list;


                for (int i = 1; i < a.video_count; i += 50) {

                    if (Thread.currentThread().isInterrupted())
                        break;

                    if (AuthorVideoObject!=null) {
                        AuthorVideoObject.clear();
                        AuthorVideoObject = null;
                    }
                    AuthorVideoObject = new ArrayList<>();

                    if (Thread.currentThread().isInterrupted())
                        return;

                    http = null;
                    http = new Socket_client();
                    http.send_request(a.url_videos + "?alt=json&start-index=" + i + "&max-results=50");
                    jsonToObject = new JsonToObjects();
                    jsonToObject.ParseAuthorVideo(AuthorVideoObject, http.get_response(), i);
                    jsonToObject = null;

                    if (Thread.currentThread().isInterrupted())
                        return;

                    MainLogic.synchronizedlogging.WriteToLog("Получение видео автора " + a.nickname + " " +i);


                    // можно оформить ввиде отдельной функции
                    if (AuthorVideoObject == null) {
                        continue;
                    }
                    for (JSONObject c : AuthorVideoObject) {
                        if (Thread.currentThread().isInterrupted())
                            break;

                        if(c == null)
                            continue;
                        String[] parts = null;
                        try{
                            parts = c.getJSONObject("id").getString("$t").split("/");
                            String id = parts[parts.length - 1];
                            if (!MainLogic.AlreadyParsedVids.contains(id)) // проверка что видио еще не обрабатывали.
                            {
                                MainLogic.SynAlreadyParsedVideos(1, id);
                                if (Depth != 0) {
                                    list.add(new OneUrlProcess(id, 2, Depth, MaxDepth, false));
                                } else {
                                    list.add(new OneUrlProcess(id, 0, 0, MaxDepth, false));
                                }
                            }
                        } catch(JSONException e){
                            SynchronizedLogging.WritetoErrorLogFile("Error making result in Author " + author);
                        }
                        finally {
                            parts = null;
                        }

                    }


                }
            }
        } else {
            //MainLogic.synchronizedlogging.WriteToLog("Ошибка получения данных об авторе " + author);
            SynchronizedLogging.WritetoErrorLogFile("Error getting author " + author);
            http = null;
            return;
        }




    }
}
