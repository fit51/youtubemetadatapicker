/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel.ParsingObjects;

import java.util.ArrayList;

/**
 *
 * @author pavel
 */
public class Author {
    public String type = "author";
    public String nickname; // Автор (Сетевой псевдоним)
    public String id;
    public String googleplus; // Имя учетной записи google+ (В намше случае ID)
    public String registration_date; // Дата регистрации
    public String text; // Текст
    public String subscriber_count; // Кол-во подписчиков
    public String url_videos; // Ссылка на видео автора. Выводится максимум 50. Поэтому надо выводить по кускам. ?start-index=1&max-results=50
    public int video_count; // Количество видео у автора
    public String url_channels; // Сссылка на каналы автора
    //public ArrayList<Channels> channels; // Каналы автора
}
