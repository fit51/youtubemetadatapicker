/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel.ParsingObjects;

import java.util.ArrayList;

/**
 *
 * @author pavel
 */
public class AuthorVideo {
    public String type = "video";
    public String title; // Название
    public String id; // id from Youtube
    public String date; // Дата добавления
    public int views; // Просмотры
    public int commentscount; // кол-во комментоа
    public String commentsurl ; // ссылка на комментарии
    public ArrayList<Comment> comments;
}
