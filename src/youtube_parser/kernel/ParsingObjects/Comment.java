/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel.ParsingObjects;

import java.util.List;

/**
 *
 * @author pavel
 */
public class Comment {

    public String id;
    public String authorid;
    public String authorname;
    public String text;
    public String date;
    public List<Comment> replies;
}
