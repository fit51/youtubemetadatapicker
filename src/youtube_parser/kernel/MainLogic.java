/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package youtube_parser.kernel;

import java.awt.Label;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;
import youtube_parser.Main;
import youtube_parser.kernel.executors.OneUrlProcess;
import youtube_parser.kernel.executors.ResultOfUrlThread;

/**
 *
 * @author pavel
 */
public class MainLogic extends SwingWorker<Void, Void> {

    private File fileurls;
    private ArrayList<String> vidioids;
    private javax.swing.JButton startbutton;
    private javax.swing.JButton stopbutton;

    /**
     *
     * Variable to throw inform to UserConsole
     */
    Socket_client http;
    private JsonToObjects JtoO;
    public int RecDepth;
    public static SynchronizedLogging synchronizedlogging;
    ExecutorService executor;
    public static boolean GoOverAuthorVidios = false;
    private int ThreadsNumber = 0;
    public static String GoogleApiKey;
    public static boolean OutGoogleQuoute = false;
    public static boolean GatherReplies = true;

    /**
     * получать или нет комментарии к видео
     */
    public static boolean getComments = true;

    public static HashSet<String> AlreadyParsedVids;
    public static HashSet<String> AlreadyParsedAuth;

    public SynchronizedLogging startProgram(boolean getComments, boolean GoOverAuthorVidio, boolean GoOverCurrentAuthorVidios, int RecDepth, int ThreadsNumber, File urls,
            JTextArea log, javax.swing.JButton startbutton, javax.swing.JButton stopbutton, Label AllQueue, Label DoneQueue,String GoogleApiKey) {

        MainLogic.getComments = getComments;

        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yy-HH.mm.ss");
        File dir = new File(sdf.format(cal.getTime()));
        if(!dir.exists())
            dir.mkdir();
        else
            return null;
        if (log!=null)
            log.setText("");
        synchronizedlogging = new SynchronizedLogging(log, sdf.format(cal.getTime()), AllQueue, DoneQueue);
        Saver.new_file(sdf.format(cal.getTime()));
        fileurls = urls;
        this.startbutton = startbutton;
        this.stopbutton = stopbutton;
        this.ThreadsNumber = ThreadsNumber;
        if(GoogleApiKey == null || GoogleApiKey.isEmpty())
            GatherReplies = false;
        MainLogic.GoogleApiKey = GoogleApiKey;
//        fileurls = new File("/home/pavel/Documents/Java/urls");

//       // synchronizedlogging.WriteToLog("Try to append 1");
//        log.append("Try to append 2\n");
        GoOverAuthorVidios = GoOverAuthorVidio;
        http = new Socket_client();
        JtoO = new JsonToObjects();
        this.RecDepth = RecDepth;
        AlreadyParsedVids = new HashSet();
        AlreadyParsedAuth = new HashSet();
        try {
            vidioids = new ArrayList<>();
            ParseFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
//        for(String s: vidioids){
//            Workwhithvidio(s);   
//        }
//        Log.append("Your vidio ids!"+"\n");
//        for(String s: vidioids){
//            Log.append(s+"\n");
//        }
        return synchronizedlogging;
    }

    public void stopProgram() {
        if(executor == null)
            return;
        executor.shutdownNow();
        if(!executor.isTerminated())
            try {
                // если не сможет закрыть executor, то программа намертво зависнет
                // так было, когда одна из задач была заблокирована
                while (!executor.awaitTermination(3, TimeUnit.SECONDS)) {
                    executor.shutdownNow();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(MainLogic.class.getName()).log(Level.SEVERE, null, ex);
                SynchronizedLogging.WritetoErrorLogFile("Error stoping program!");
            }

        Saver.save_end();
    }
    
    public static synchronized boolean SynAlreadyParsedAuthors(int type, String id){
        if(type == 0)
            return MainLogic.AlreadyParsedAuth.contains(id);
        else
            MainLogic.AlreadyParsedAuth.add(id);
        return true;
    }
//    
    public static synchronized boolean SynAlreadyParsedVideos(int type, String id){
        if(type == 0)
            return MainLogic.AlreadyParsedVids.contains(id);
        else
            MainLogic.AlreadyParsedVids.add(id);
        return true;
    }

    public void MultithreadWork(ArrayList<String> startVideolist) {
        if(ThreadsNumber == 0)
            executor = Executors.newCachedThreadPool();
        else
            executor= Executors.newFixedThreadPool(ThreadsNumber);
//        executor = Executors.newFixedThreadPool(n);
        CompletionService<ResultOfUrlThread> pool = new ExecutorCompletionService<>(executor);
//        ConcurrentLinkedQueue<Future<ResultOfUrlThread>> list = new ConcurrentLinkedQueue();
        int SubmittedTasks = 0;
        for (String vidio : startVideolist) {
//            AlreadyParsedVids.add(vidio);
            Callable<ResultOfUrlThread> callable = new OneUrlProcess(vidio, 0, 0, RecDepth, true);
            pool.submit(callable);
           synchronizedlogging.WriteNumber(-1, 0);
            SubmittedTasks++;

        }

        //TODO наверное можно было сделать проще, тогда бы и задачи сразу закрывались

        LinkedList<OneUrlProcess> q = new LinkedList<>();
        for (int tasksHandled = 0; tasksHandled < SubmittedTasks && !executor.isShutdown(); tasksHandled++) {

            try {
                if(ThreadsNumber != 0) {
                    Future<ResultOfUrlThread> result = pool.take();



                    ResultOfUrlThread r =  result.get();
    //                   synchronizedlogging.WriteToLog(r.Message);
                    for (OneUrlProcess p : r.list) {
                        q.add(p);
                    }
                    while(SubmittedTasks - tasksHandled < ThreadsNumber && !q.isEmpty() && !executor.isShutdown()){

                        OneUrlProcess p = q.poll();
                        pool.submit(p);
                       synchronizedlogging.WriteNumber(-1, 0);
                        SubmittedTasks++;
                    }
                } else {
                    Future<ResultOfUrlThread> result = pool.take();
                    if (executor.isShutdown())
                        result.cancel(true);

                     ResultOfUrlThread r = result.get();

                    for(OneUrlProcess p: r.list){

                        pool.submit(p);
                       synchronizedlogging.WriteNumber(-1, 0);
                        SubmittedTasks++;
                     }
                }
            } catch (InterruptedException e) {
               synchronizedlogging.WriteToLog("Ошибка прерывания потоков");
            } catch (ExecutionException e) {
               synchronizedlogging.WriteToLog("Error get() threw exception");
                synchronizedlogging.WriteToLog(e.toString());
            }
        }
        //shut down the executor service now
        executor.shutdown();
       synchronizedlogging.WriteToLog("Конец!");
        if(!Main.inCommandLineMod){
            startbutton.setEnabled(true);
            stopbutton.setEnabled(false);
        }
    }

//    public void Workwhithvidio(String vidio){
//        if(!AlreadyParsedVids.contains(vidio)){
//           // synchronizedlogging.WriteToLog("Working with "+vidio);
//              // synchronizedlogging.WriteToLog("Working with "+vidio);
//                if(http.send_request(YoutubeApiv2.GetVideoInfo(vidio)) == 1){
//                   // synchronizedlogging.WriteToLog("Success geting "+vidio);
//                    String vidiodata = http.get_response();
//                    VidioObject v = JtoO.ParseVideo(vidiodata, vidio);
//                   // synchronizedlogging.WriteToLog(v.Title+"\n");
//                    AlreadyParsedVids.add(vidio);
//                    
//                } else
//                   // synchronizedlogging.WriteToLog("Error getting "+vidio);
//        }
//    }
    public void ParseFile() throws FileNotFoundException {
        if(fileurls == null)
            return;
        BufferedReader in = new BufferedReader(new FileReader(fileurls));

        try {
            while (in.ready()) {
                String s = in.readLine();
                if(!s.contains("www.youtube.com/watch?v=")){
                   synchronizedlogging.WriteToLog("Неверные входные данные: "+s);
                    continue;
                }
                int i1 = s.indexOf("/watch?v=")+9;
                int i2 = s.lastIndexOf("&");
                if(i2 == -1)
                    i2 = s.length();
                s = s.substring(i1, i2);
                vidioids.add(s);
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(MainLogic.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected Void doInBackground() {
        MultithreadWork(vidioids);
        Saver.save_end();
        return null;
    }

    @Override
    protected void done() {

    }
}
